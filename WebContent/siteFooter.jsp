<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<hr />
		<footer>
			<p>&copy; Powered by ADS 2015 Group 2</p>
		</footer>
		</div><!--/.container-->
		<script type="text/javascript" src="<c:url value='/libs/jquery/jquery.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/libs/jquery/dataTables/jquery.dataTables.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/libs/bootstrap/js/bootstrap.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/libs/bootstrap/js/bootstrap-datepicker.min.js'/>"></script>
<%-- 		<script type="text/javascript" src="<c:url value='/libs/bootstrap/js/validator.min.js'/>"></script> --%>
		<script type="text/javascript" src="<c:url value='/js/default.js'/>"></script>
	</body>
</html>