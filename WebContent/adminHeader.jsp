<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Admin${pageTitle}</title>
<link href="<c:url value="/libs/bootstrap/css/bootstrap.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/libs/bootstrap/css/bootstrap-datepicker.min.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/libs/jquery/dataTables/jquery.dataTables.min.css"/>"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/css/default.css"/>" rel="stylesheet"
	type="text/css" />

<!-- HTML5 shim e Respond.js para suporte no IE8 de elementos HTML5 e media queries -->
<!-- ALERTA: Respond.js n�o funciona se voc� visualizar uma p�gina file:// -->
<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<c:url value="/admin"/>">Admin</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li ${activeHome}><a href="<c:url value="/admin"/>">Home</a></li>
					<li ${activeCategory}><a
						href="<c:url value="/admin/category"/>">Categoria(s)</a></li>
					<li ${activeUser}><a href="<c:url value="/admin/user"/>">Usu�rio(s)</a></li>
					<li ${activeProfile}><a href="<c:url value="/admin/profile"/>">Perfil(es)</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<c:url value="/logout"/>">Sair</a></li>
				</ul>
			</div>
		</div>
	</nav>