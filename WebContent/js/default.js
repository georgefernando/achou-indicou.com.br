/*******************************************************************************
 * 
 */

$(document).ready(function() {
	$('.datepicker').datepicker();

	// Tables - begin
	$('#tblSearchResult').dataTable();
	$('#tblCategory').dataTable();
	$('#tblProfile').dataTable();
	$('#tblComment').dataTable();
	$('#tblUser').dataTable();
	// Tables - end

	if ($('#profile').val() == 'Selecionar' || $('#profile').val() != 2) {
		$("#txtUserDescription").prop("disabled", true);
		$("#category").prop("disabled", true);
	} else {
		$("#txtUserDescription").prop("disabled", false);
		$("#category").prop("disabled", false);
	}

	$(document).on('change', '#profile', function() {
		if ($('#profile').val() == 2) {
			$("#txtUserDescription").prop("disabled", false);
			$("#txtUserDescription").prop("required", true);
			$("#category").prop("disabled", false);
			$("#category").prop("required", true);
		} else {
			$("#txtUserDescription").prop("disabled", true);
			$("#txtUserDescription").prop("required", false);
			$("#txtUserDescription").val('');
			$("#category").val('Selecionar');
			$("#category").prop("disabled", true);
			$("#category").prop("required", false);
		}
	});

	$('#commentModal').on('show.bs.modal', function(event) {
		var button = $(event.relatedTarget);
		var recipient = button.data('whatever');
		var modal = $(this);
	})

	$("#btnAddComment").click(function() {
		$("#frmComment").submit();
	});
});
