<%@ include file="../../../siteHeader.jsp"%>
<div class="row row-offcanvas row-offcanvas-right">
	<div class="col-xs-12 col-sm-12">
		<div class="jumbotron" style="background-image: url('img/unnamed.png');background-repeat:no-repeat;height: 380px;">
			<form style="margin-left: 324px;    margin-top: 164px;" action="<c:url value="/search"/>"
				class="navbar-form navbar-left" method="post">
				<div class="form-group">
					<input style=" height: 42px; width: 383px;" type="text" class="form-control" placeholder="Insira os dados do profissional que deseja encontrar" id="txtSearch" name="searchText" />
				</div>
				<button type="submit" class="btn btn-default"><img src="img/lupa.png"/ width=28px> Buscar</button>
			</form>
		</div>
		<div class="row">
			<c:forEach items="${userList}" var="user">
				<div class="col-xs-6 col-lg-4">
					<h3>${user.name}</h3>
					<p>${fn:substring(user.description, 0, 250)}...</p>
					<p>
						<a class="btn btn-secondary" href="<c:url value="/user/profile/${user.id}"/>" role="button">Ver perfil &raquo;</a>
					</p>
				</div>
			</c:forEach>
		</div>
		<!--/row-->
	</div>
	<!--/span-->
</div>
<!--/row-->
<%@ include file="../../../siteFooter.jsp"%>