<%@ include file="../../../siteHeader.jsp"%>
<div class="row row-offcanvas row-offcanvas-right">
	<div class="col-xs-12 col-sm-12">
		<div class="page-header">
			<h1>Resultato da Consulta</h1>
		</div>
		<div class="table-responsive">
			<table class="table table-striped table-condensed"
				id="tblSearchResult">
				<thead>
					<tr>
						<th>Nome</th>
						<th>Endere�o</th>
						<th>A��es</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${userDTOList}" var="user">
						<tr>
							<td class="col-md-3">${user.name}</td>
							<td class="col-md-8">${user.address}</td>
							<td><a href="<c:url value="/user/profile/${user.id}"/>"
								class="btn btn-primary btn-xs"><span
									class="glyphicon glyphicon-search"></span> Ver profile</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
<%@ include file="../../../siteFooter.jsp"%>