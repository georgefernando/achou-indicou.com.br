<%@ include file="../../../siteHeader.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<div class="jumbotron">
		<form style="width:500px;" action="<c:url value="/logon"/>" class="form-signin"
			method="post">
			<h2 class="form-signin-heading">Login</h2>
			<label for="inputEmail" class="sr-only">E-mail</label> <input
				type="email" id="email" name="email" class="form-control"
				placeholder="E-mail" required autofocus /> <label
				for="inputPassword" class="sr-only">Senha</label> <input
				type="password" id="password" name="password" class="form-control"
				placeholder="Senha" required />
			<button class="btn btn-lg btn-primary btn-block" type="submit">Logar</button>
		</form>
	</div>

<%@ include file="../../../siteFooter.jsp"%>