<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../adminHeader.jsp"%>
<div class="container">
	<div class="row">
		<div class="page-header">
			<h1>Gerenciar ${pageTitle}</h1>
		</div>
	</div>
	<div class="row">
		<a href="<c:url value="/admin/category/add"/>" class="btn btn-primary">Adicionar</a>
	</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="table-responsive">
			<table class="table table-striped table-condensed" id="tblCategory">
				<thead>
					<tr>
						<th>Id</th>
						<th>Nome</th>
						<th>Descri��o</th>
						<th>Status</th>
						<th>A��es</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${categoryDTOList}" var="category">
						<tr>
							<td>${category.id}</td>
							<td>${category.name}</td>
							<td>${category.description}</td>
							<td>
							<c:choose>
								<c:when test="${category.active}">Ativa</c:when>
								<c:otherwise>Inativa</c:otherwise>
							</c:choose>
							</td>
							<td>
								<a href="<c:url value="/admin/category/edit/${category.id}"/>"
									class="btn btn-primary btn-xs">Editar</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
<%@ include file="../../../adminFooter.jsp"%>
