<%@ include file="../../../adminHeader.jsp"%>
<div class="container">
	<div class="row">
		<div class="page-header">
			<h1>${formPageTitle}</h1>
		</div>
		<form action="<c:url value="/admin/category/save"/>" method="post"
			class="form-horizontal">
			<input id="id" name="categoryDTO.id" type="hidden"
				value="${categoryDTO.id}" />
			<div class="row">
				<label for="categoryDTO.name" class="col-sm-2 control-label">Nome:</label>
				<div class="form-group col-md-5">
					<input type="text" class="form-control" id="name"
						name="categoryDTO.name" maxlength="75" value="${categoryDTO.name}" />
				</div>
			</div>
			<div class="row">
				<label for="categoryDTO.description" class="col-sm-2 control-label">Descri��o:</label>
				<div class="form-group col-md-6">
					<input type="text" class="form-control" id="description"
						name="categoryDTO.description" maxlength="100"
						value="${categoryDTO.description}" />
				</div>
			</div>
			<div class="row">
				<label for="categoryDTO.active" class="col-sm-2 control-label">Ativo:</label>
				<div class="form-group col-md-5">
					<input type="checkbox" id="active" name="categoryDTO.active"
						<c:if test="${categoryDTO.active}">checked="checked"</c:if> />
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn btn-primary">Salvar</button>
					&nbsp; &nbsp; <a href="<c:url value="/admin/category"/>"
						class="btn btn-default">Cancelar</a>
				</div>
			</div>
		</form>
	</div>
</div>
<%@ include file="../../../adminFooter.jsp"%>
