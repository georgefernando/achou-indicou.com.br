<%@ include file="../../../adminHeader.jsp"%>
<div class="container">
	<div class="row">
		<div class="page-header">
			<h1>${formPageTitle}</h1>
		</div>
	</div>
	<div class="row">
		<a href="<c:url value="/admin/user"/>" class="btn btn-primary">Voltar</a>
	</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="table-responsive">
			<table class="table table-striped table-condensed" id="tblComment">
				<thead>
					<tr>
						<th>Usu�rio</th>
						<th>Classifica��o</th>
						<th>Comentado em</th>
						<th>Coment�rio</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${commentDTOList}" var="comment">
						<tr>
							<td>${comment.userComment.name}</td>
							<td><c:choose>
									<c:when test="${comment.classification eq \"G\"}">Bom</c:when>
									<c:otherwise>Ruim</c:otherwise>
								</c:choose></td>
							<td><fmt:formatDate pattern="dd/MM/yyyy"
									value="${comment.dateComment}" /></td>
							<td>${comment.comment}</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<a href="<c:url value="/admin/user"/>" class="btn btn-primary">Voltar</a>
	</div>
</div>
<%@ include file="../../../adminFooter.jsp"%>