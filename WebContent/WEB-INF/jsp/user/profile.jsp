<%@ include file="../../../siteHeader.jsp"%>
<div class="row row-offcanvas row-offcanvas-right">
	<div class="col-xs-12 col-sm-12">
		<div class="page-header">
			<h1>${userDTO.name}</h1>
		</div>
		<p>${userDTO.description}</p>
		<hr />
		<div class="panel panel-default">
			<div class="panel-heading">Dados do Profissional</div>
			<div class="panel-body">
				<address>
					<strong>Endereço</strong><br /> ${userDTO.address}<br /> <abbr
						title="Phone"><strong>Phone:</strong></abbr> ${userDTO.telephone} <br /> <abbr
						title="Cell"><strong>Cell:</strong></abbr> ${userDTO.cellphone}<br /> <abbr
						title="Email"><strong>E-mail:</strong></abbr> <a href="mailto:${userDTO.email}">${userDTO.email}</a>
				</address>
				<address>
					<strong>Categoria do Profissional</strong><br />
					${userDTO.category.name}
				</address>

				<strong>Classificação</strong><br />
				<div class="progress">
					<div class="progress-bar progress-bar-success"
						style="width: ${userDTO.good}%">
						<fmt:formatNumber pattern="0.##" value="${userDTO.good}" />%
					</div>
					<div class="progress-bar progress-bar-danger"
						style="width: ${userDTO.bad}%">
						<fmt:formatNumber pattern="0.##" value="${userDTO.bad}" />%
					</div>
					<div class="progress-bar progress-bar-info" style="width: ${userDTO.neutral}%">
						<fmt:formatNumber pattern="0.##" value="${userDTO.neutral}" />%
					</div>
				</div>
			</div>
		</div>
		<c:if test="${userIsloggeg}">
			<p>
				<button type="button" class="btn btn-primary" data-toggle="modal"
					data-target="#commentModal" data-whatever="@mdo">Comentar</button>
			</p>
		</c:if>
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Comentário(s)</div>
			<!-- Table -->
			<div class="table-responsive">
				<table class="table table-striped table-condensed">
					<thead>
						<tr>
							<th>Quem comentou</th>
							<th>Classificação</th>
							<th>Comentado em</th>
							<th>Comentário</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${commentList}" var="comment">
							<tr>
								<td class="col-md-4">${comment.userComment.name}</td>
								<td class="col-md-1"><c:choose>
										<c:when test="${comment.classification eq \"G\"}">Bom</c:when>
										<c:when test="${comment.classification eq \"B\"}">Ruim</c:when>
										<c:otherwise>Netro</c:otherwise>
									</c:choose></td>
								<td class="col-md-2"><fmt:formatDate pattern="dd/MM/yyyy"
										value="${comment.dateComment}" /></td>
								<td>${comment.comment}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

		<div class="modal fade" id="commentModal" tabindex="-1" role="dialog"
			aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="exampleModalLabel">New message</h4>
					</div>
					<div class="modal-body">
						<form method="post" action="<c:url value="/user/comment/save"/>"
							id="frmComment">
							<input type="hidden" value="${userDTO.id}" id="userId"
								name="userId" />
							<div class="form-group">
								<label for="classification" class="control-label">Classificação:</label>
								<select id="classification" name="classification">
									<option value="N">Neutro</option>
									<option value="G">Bom</option>
									<option value="B">Ruim</option>
								</select>
							</div>
							<div class="form-group">
								<label for="comment" class="control-label">Comentario:</label>
								<textarea class="form-control" id="comment" name="comment"></textarea>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<button type="button" id="btnAddComment" class="btn btn-primary">Comentar</button>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<%@ include file="../../../siteFooter.jsp"%>