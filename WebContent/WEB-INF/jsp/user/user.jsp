<%@ include file="../../../siteHeader.jsp"%>
<div class="container">
	<div class="row">
		<div class="page-header">
			<h1>${formPageTitle}</h1>
		</div>
		<form action="<c:url value="/user/save"/>" method="post"
			class="form-horizontal">
			<input id="id" name="userDTO.id" type="hidden" value="${userDTO.id}" />
			<div class="row">
				<label for="userDTO.name" class="col-sm-2 control-label">Nome:</label>
				<div class="form-group col-md-6">
					<input type="text" class="form-control" id="name"
						name="userDTO.name" maxlength="100" value="${userDTO.name}" />
				</div>
			</div>
			<div class="row">
				<label for="userDTO.email" class="col-sm-2 control-label">E-mail:</label>
				<div class="form-group col-md-6">
					<input type="text" class="form-control" id="email"
						name="userDTO.email" maxlength="75" value="${userDTO.email}" />
				</div>
			</div>
			<div class="row">
				<label for="userDTO.address" class="col-sm-2 control-label">Endere�o:</label>
				<div class="form-group col-md-6">
					<input type="text" class="form-control" id="address"
						name="userDTO.address" maxlength="75" value="${userDTO.address}" />
				</div>
			</div>
			<div class="row">
				<label for="userDTO.documentNumber" class="col-sm-2 control-label">N�.
					Documento:</label>
				<div class="form-group col-md-2">
					<input type="text" class="form-control" id="documentNumber"
						name="userDTO.documentNumber" maxlength="14"
						value="${userDTO.documentNumber}" /> <small
						id="documentNumberHelp" class="form-text text-muted">CNPJ
						ou CPF sem "." ou "-".</small>
				</div>
			</div>
			<div class="row">
				<label for="profileDTO.id" class="col-sm-2 control-label">Perfil:</label>
				<div class="form-group col-md-2">
					<select class="selectpicker" id="profile" name="profileDTO.id">
						<option>Selecionar</option>
						<c:forEach items="${profileDTOList}" var="profile">
							<option value="${profile.id}"
								<c:if test="${profile.id eq userDTO.profile.id}">selected="selected"</c:if>>${profile.name}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="row">
				<label for="categoryDTO.id" class="col-sm-2 control-label">Categoria(s):</label>
				<div class="form-group col-md-5">
					<select class="selectpicker" id="category" name="categoryDTO.id">
						<option>Selecionar</option>
						<c:forEach items="${categoryDTOList}" var="category">
							<option value="${category.id}"
								<c:if test="${category.id eq userDTO.category.id}">selected="selected"</c:if>>${category.name}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="row">
				<label for="userDTO.birthdate" class="col-sm-2 control-label">Data
					Nascimento:</label>
				<div class="form-group col-md-2">
					<input type="text" class="form-control datepicker" id="birthdate"
						name="userDTO.birthdate" maxlength="10"
						data-date-format="dd/mm/yyyy"
						value="<fmt:formatDate pattern="dd/MM/yyyy" value="${userDTO.birthdate}" />" />
				</div>
			</div>
			<div class="row">
				<label for="userDTO.telephone" class="col-sm-2 control-label">Telefone:</label>
				<div class="form-group col-md-2">
					<input type="text" class="form-control" id="telephone"
						name="userDTO.telephone" maxlength="20"
						value="${userDTO.telephone}" />
				</div>
			</div>
			<div class="row">
				<label for="userDTO.cellphone" class="col-sm-2 control-label">Celular:</label>
				<div class="form-group col-md-2">
					<input type="text" class="form-control" id="cellphone"
						name="userDTO.cellphone" maxlength="20"
						value="${userDTO.cellphone}" />
				</div>
			</div>
			<div class="row">
				<label for="userDTO.password" class="col-sm-2 control-label">Senha:</label>
				<div class="form-group col-md-2">
					<input type="password" class="form-control" id="password"
						name="userDTO.password" maxlength="12" value="${userDTO.password}" />
				</div>
			</div>
			<div class="row">
				<label for="userDTO.active" class="col-sm-2 control-label">Ativo:</label>
				<div class="form-group col-md-5">
					<input type="checkbox" id="active" name="userDTO.active"
						<c:if test="${userDTO.active}">checked="checked"</c:if> />
				</div>
			</div>
			<div class="row">
				<label for="userDTO.active" class="col-sm-2 control-label">Descri��o:</label>
				<div class="form-group col-md-6">
					<textarea class="form-control" id="txtUserDescription"
						maxlength="3000" name="userDTO.description" rows="10">${userDTO.description}</textarea>
				</div>
			</div>
			<div class="row">&nbsp;</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn btn-primary">Salvar</button>
					&nbsp; &nbsp; <a href="<c:url value="/admin/user"/>"
						class="btn btn-default">Cancelar</a>
				</div>
			</div>
		</form>
	</div>
</div>
<%@ include file="../../../siteFooter.jsp"%>
