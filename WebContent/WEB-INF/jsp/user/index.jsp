<%@ include file="../../../adminHeader.jsp"%>
<div class="container">
	<div class="row">
		<div class="page-header">
			<h1>Gerenciar ${pageTitle}</h1>
		</div>
	</div>
	<div class="row">
		<a href="<c:url value="/admin/user/add"/>" class="btn btn-primary">Adicionar</a>
	</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="table-responsive">
			<table class="table table-striped table-condensed" id="tblUser">
				<thead>
					<tr>
						<th>Id</th>
						<th>Nome</th>
						<th>E-mail</th>
						<th>Data Nascimento</th>
						<th>Perfil</th>
						<th>Categoria</th>
						<th>Status</th>
						<th>A��es</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${userDTOList}" var="user">
						<tr>
							<td>${user.id}</td>
							<td>${user.name}</td>
							<td>${user.email}</td>
							<td><fmt:formatDate pattern="dd/MM/yyyy"
									value="${user.birthdate}" /></td>
							<td>${user.profile.name}</td>
							<td><c:choose>
									<c:when test="${empty user.category.id}">-</c:when>
									<c:otherwise>${user.category.name}</c:otherwise>
								</c:choose></td>
							<td><c:choose>
									<c:when test="${user.active}">Ativa</c:when>
									<c:otherwise>Inativa</c:otherwise>
								</c:choose></td>
							<td><a href="<c:url value="/admin/user/edit/${user.id}"/>"
								class="btn btn-primary btn-xs">Editar</a> <c:if
									test="${user.profile.name eq \"Profissional\"}">
									<a href="<c:url value="/admin/user/comment/${user.id}"/>"
										class="btn btn-primary btn-xs">Coment�rio(s)</a>
								</c:if></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
<%@ include file="../../../adminFooter.jsp"%>
