<%@ include file="../../../adminHeader.jsp"%>
<div class="container">
	<div class="row">
		<div class="page-header">
			<h1>${formPageTitle}</h1>
		</div>
		<form action="<c:url value="/admin/profile/save"/>" method="post"
			class="form-horizontal">
			<input id="id" name="profileDTO.id" type="hidden"
				value="${profileDTO.id}" />
			<div class="row">
				<label for="profileDTO.name" class="col-sm-2 control-label">Nome:</label>
				<div class="form-group col-md-5">
					<input type="text" class="form-control" id="name"
						name="profileDTO.name" maxlength="75" value="${profileDTO.name}" />
				</div>
			</div>
			<div class="row">
				<label for="profileDTO.active" class="col-sm-2 control-label">Ativo:</label>
				<div class="form-group col-md-5">
					<input type="checkbox" id="active" name="profileDTO.active"
						<c:if test="${profileDTO.active}">checked="checked"</c:if> />
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn btn-primary">Salvar</button>
					&nbsp; &nbsp; <a href="<c:url value="/admin/profile"/>" class="btn btn-default">Cancelar</a>
				</div>
			</div>
		</form>
	</div>
</div>
<%@ include file="../../../adminFooter.jsp"%>
