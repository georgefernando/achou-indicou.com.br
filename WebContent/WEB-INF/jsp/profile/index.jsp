<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../../adminHeader.jsp"%>
<div class="container">
	<div class="row">
		<div class="page-header">
			<h1>Gerenciar ${pageTitle}</h1>
		</div>
	</div>
	<div class="row">
		<a href="<c:url value="/admin/profile/add"/>" class="btn btn-primary">Adicionar</a>
	</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="table-responsive">
			<table class="table table-striped table-condensed" id="tblProfile">
				<thead>
					<tr>
						<th>Id</th>
						<th>Nome</th>
						<th>Status</th>
						<th>A��es</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${profileDTOList}" var="profile">
						<tr>
							<td>${profile.id}</td>
							<td>${profile.name}</td>
							<td>
							<c:choose>
								<c:when test="${profile.active}">Ativa</c:when>
								<c:otherwise>Inativa</c:otherwise>
							</c:choose>
							</td>
							<td>
								<a href="<c:url value="/admin/profile/edit/${profile.id}"/>"
									class="btn btn-primary btn-xs">Editar</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
<%@ include file="../../../adminFooter.jsp"%>
