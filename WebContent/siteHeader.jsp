<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta http-equiv="Content-Type" content="text/html ; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>${pageTitle}</title>
<link href="<c:url value="/libs/bootstrap/css/bootstrap.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/libs/bootstrap/css/bootstrap-datepicker.min.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/libs/jquery/dataTables/jquery.dataTables.min.css"/>"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/css/offcanvas.css"/>" rel="stylesheet"
	type="text/css" />
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="<c:url value="/"/>"> <img
					src="<c:url value="/img/achouIndicouText.png"/>" width="186px"
					height="35px" /></a>
			</div>
			<form action="<c:url value="/search"/>"
				class="navbar-form navbar-left" method="post">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Search"
						id="txtSearch" name="searchText" />
				</div>
				<button type="submit" class="btn btn-default">Buscar</button>
			</form>
			<ul class="nav navbar-nav navbar-right">
				<c:choose>
					<c:when test="${not userIsloggeg}">
						<li><a href="<c:url value="/user/register"/>"><span
								class="glyphicon glyphicon-user"></span> Sign Up</a></li>
						<li><a href="<c:url value="/login"/>"><span
								class="glyphicon glyphicon-log-in"></span> Login</a></li>
					</c:when>
					<c:otherwise>

						<li><a href="<c:url value="/user/${userSession.id}"/>"><span
								class="glyphicon glyphicon-user"></span> ${userSession.name}</a></li>
						<li><a href="<c:url value="/logout"/>"><span
								class="glyphicon glyphicon-log-out"></span> Logout</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>
	</nav>
	<div class="container">