<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<p>&nbsp;</p>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">Powered by Group 2</p>
			</div>
		</footer>
		<script type="text/javascript" src="<c:url value='/libs/jquery/jquery.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/libs/jquery/dataTables/jquery.dataTables.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/libs/bootstrap/js/bootstrap.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/libs/bootstrap/js/bootstrap-datepicker.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/js/default.js'/>"></script>
	</body>
</html>