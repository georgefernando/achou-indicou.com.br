package br.com.achouindicou.dto;

import java.io.Serializable;
import java.util.Date;

public class CommentDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	private String comment;
	private String classification;
	private UserDTO user;
	private UserDTO userComment;
	private Date dateComment;
	
	/**
	 * Getters and Setters.
	 */
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}
	
	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}
	
	public UserDTO getUserComment() {
		return userComment;
	}

	public void setUserComment(UserDTO userComment) {
		this.userComment = userComment;
	}
	
	public Date getDateComment() {
		return dateComment;
	}

	public void setDateComment(Date dateComment) {
		this.dateComment = dateComment;
	}

}
