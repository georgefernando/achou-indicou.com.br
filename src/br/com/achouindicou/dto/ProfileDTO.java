package br.com.achouindicou.dto;

import java.io.Serializable;

public class ProfileDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;

	private String name;

	private boolean active;

	
	/**
	 * Getters and Setter.
	 */
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
