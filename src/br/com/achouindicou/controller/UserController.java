package br.com.achouindicou.controller;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import br.com.achouindicou.bll.ICategoryBLL;
import br.com.achouindicou.bll.ICommentBLL;
import br.com.achouindicou.bll.IProfileBLL;
import br.com.achouindicou.bll.IUserBLL;
import br.com.achouindicou.dto.CategoryDTO;
import br.com.achouindicou.dto.CommentDTO;
import br.com.achouindicou.dto.ProfileDTO;
import br.com.achouindicou.dto.UserDTO;
import br.com.achouindicou.interceptor.Logged;
import br.com.achouindicou.interceptor.Public;
import br.com.achouindicou.service.UserSession;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;

@Controller
public class UserController {
	@Inject
	private Result result;

	@Inject
	private IUserBLL userBLL;

	@Inject
	private ICategoryBLL categoryBLL;

	@Inject
	private IProfileBLL profileBLL;

	@Inject
	private ICommentBLL commentBLL;

	@Inject
	private UserSession userSession;

	@Inject
	public UserController(IUserBLL userBLL, ICategoryBLL categoryBLL, IProfileBLL profileBLL, ICommentBLL commentBLL,
			Result result, UserSession userSession) {
		this.categoryBLL = categoryBLL;
		this.profileBLL = profileBLL;
		this.userBLL = userBLL;
		this.result = result;
		this.commentBLL = commentBLL;
		this.userSession = userSession;
	}

	protected UserController() {
		this(null, null, null, null, null, null);
	}

	@Get("/admin/user")
	public List<UserDTO> index() {
		result.include("pageTitle", " - Usuário(s)");
		result.include("activeUser", "class=\"active\"");
		this.result.include("userIsloggeg", this.userSession.isLogged());
		this.result.include("userSession", this.userSession.getUser());

		return this.userBLL.get();
	}

	@Get("/admin/user/add")
	public void form() {
		result.include("pageTitle", " - Adicionar Usuário");
		result.include("formPageTitle", "Adicionar Usuário");
		result.include("activeUser", "class=\"active\"");
		result.include("profileDTOList", this.profileBLL.getActives());
		result.include("categoryDTOList", this.categoryBLL.getActives());
		this.result.include("userIsloggeg", this.userSession.isLogged());
		this.result.include("userSession", this.userSession.getUser());
	}

	@Get("/admin/user/edit/{id}")
	public UserDTO form(int id) {
		result.include("pageTitle", " - Editar Usuário");
		result.include("activeUser", "class=\"active\"");
		result.include("profileDTOList", this.profileBLL.getActives());
		result.include("categoryDTOList", this.categoryBLL.getActives());
		this.result.include("userIsloggeg", this.userSession.isLogged());
		this.result.include("userSession", this.userSession.getUser());

		UserDTO user = this.userBLL.get(id);

		result.include("formPageTitle", "Editar Usuário - " + user.getName());

		return user;
	}

	@Get("/admin/user/comment/{id}")
	public List<CommentDTO> comment(int id) {
		result.include("pageTitle", " - Comentátio(s)");
		result.include("formPageTitle", "Comentátio(s)");
		result.include("activeUser", "class=\"active\"");
		this.result.include("userIsloggeg", this.userSession.isLogged());
		this.result.include("userSession", this.userSession.getUser());

		return this.commentBLL.get();
	}

	@Post("admin/user/save")
	@Public
	public void save(UserDTO userDTO, ProfileDTO profileDTO, CategoryDTO categoryDTO) {
		userDTO.setProfile(this.profileBLL.get(profileDTO.getId()));

		if (profileDTO.getId() == 2) {
			userDTO.setCategory(this.categoryBLL.get(categoryDTO.getId()));
		} else {
			userDTO.setCategory(null);
		}

		this.userBLL.save(userDTO);

		result.redirectTo(this).index();
	}

	@Get("user/register")
	@Public
	public void register() {
		List<ProfileDTO> profiles = this.profileBLL.getActives();

		profiles.remove(0);

		result.include("pageTitle", "Achou Indicou - Cadastro de Usuário");
		result.include("formPageTitle", "Adicionar Usuário");
		result.include("profileDTOList", profiles);
		result.include("categoryDTOList", this.categoryBLL.getActives());
		this.result.include("userIsloggeg", this.userSession.isLogged());
		this.result.include("userSession", this.userSession.getUser());
	}

	@Get("user/profile/{id}")
	@Public
	public UserDTO profile(long id) {
		UserDTO user = this.userBLL.get(id);

		this.result.include("pageTitle", "Achou Indicou - Perfil " + user.getName());
		this.result.include("commentList", this.commentBLL.getCommentsByUser(id));
		this.result.include("userIsloggeg", this.userSession.isLogged());
		this.result.include("userSession", this.userSession.getUser());

		return user;
	}

	@Logged
	@Post("user/comment/save")
	@Public
	public void userCommentSave(long userId, String classification, String comment) {
		CommentDTO userComment = new CommentDTO();

		userComment.setClassification(classification);
		userComment.setComment(comment);
		userComment.setUserComment(this.userBLL.get(this.userSession.getUser().getId()));
		userComment.setUser(this.userBLL.get(userId));
		userComment.setDateComment(new Date());

		this.commentBLL.save(userComment);

		result.redirectTo(this).profile(userId);
	}

	@Get("/user/{id}")
	@Logged
	@Public	
	public UserDTO user(long id) {
		List<ProfileDTO> profiles = this.profileBLL.getActives();
		profiles.remove(0);
		result.include("pageTitle", " - Editar Usuário");
		result.include("profileDTOList", profiles);
		result.include("categoryDTOList", this.categoryBLL.getActives());
		this.result.include("userIsloggeg", this.userSession.isLogged());
		this.result.include("userSession", this.userSession.getUser());

		UserDTO user = this.userBLL.get(id);

		result.include("formPageTitle", "Editar Usuário - " + user.getName());
	
		return user;
	}
	
	@Logged
	@Post("/user/save")
	@Public
	public void userSave(UserDTO userDTO, ProfileDTO profileDTO, CategoryDTO categoryDTO) {
		userDTO.setProfile(this.profileBLL.get(profileDTO.getId()));

		if (profileDTO.getId() == 2) {
			userDTO.setCategory(this.categoryBLL.get(categoryDTO.getId()));
		} else {
			userDTO.setCategory(null);
		}

		this.userBLL.save(userDTO);

		result.redirectTo(HomeController.class).index();
	}
}
