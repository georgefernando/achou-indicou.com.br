package br.com.achouindicou.controller;

import java.util.List;

import javax.inject.Inject;

import br.com.achouindicou.bll.IUserBLL;
import br.com.achouindicou.dto.UserDTO;
import br.com.achouindicou.interceptor.Public;
import br.com.achouindicou.service.UserSession;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;

@Controller
public class HomeController {
	@Inject
	private Result result;

	@Inject
	private UserSession userSession;

	private IUserBLL userBLL;

	HomeController() {
		this(null, null, null);
	}

	@Inject
	public HomeController(UserSession userSession, IUserBLL userBLL, Result result) {
		this.result = result;
		this.userBLL = userBLL;
		this.userSession = userSession;
	}

	@Get("/")
	@Public
	public void index() {
		List<UserDTO> users = this.userBLL.getByProfiles(2);

		this.result.include("pageTitle", "Achou Indicou - O sua opção de procura de mêcanicos profissionais.");
		this.result.include("userIsloggeg", this.userSession.isLogged());
		this.result.include("userSession", this.userSession.getUser());
		this.result.include("userList", users);
	}

	@Post("/search")
	@Public
	public List<UserDTO> result(String searchText) {
		this.result.include("userIsloggeg", this.userSession.isLogged());
		this.result.include("userSession", this.userSession.getUser());

		List<UserDTO> users = this.userBLL.getByText(searchText);

		if (users == null || users.isEmpty()) {
			return this.userBLL.getByProfiles(2);
		}

		return users;
	}
}
