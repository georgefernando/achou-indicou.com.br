package br.com.achouindicou.controller;

import javax.inject.Inject;

import br.com.achouindicou.bll.IUserBLL;
import br.com.achouindicou.dto.UserDTO;
import br.com.achouindicou.interceptor.Public;
import br.com.achouindicou.service.UserSession;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;

@Controller
public class LoginController {

	@Inject
	private Result result;

	@Inject
	private UserSession userSession;

	@Inject
	private IUserBLL userBLL;

	LoginController() {
		this(null, null, null);
	}

	@Inject
	public LoginController(UserSession userSession, IUserBLL userBLL, Result result) {
		this.result = result;
		this.userBLL = userBLL;
		this.userSession = userSession;
	}

	@Get("/login")
	@Public
	public void index() {
	}

	@Post("/logon")
	@Public
	public void logon(String email, String password) {
		UserDTO user = this.userBLL.logon(email, password);

		if (user != null) {
			this.userSession.logon(user);

			if (user.getProfile().getName().toLowerCase().equals("administrador")) {
				this.result.redirectTo(AdminController.class).index();
			} else {
				this.result.redirectTo(HomeController.class).index();
			}

			return;
		}

		this.result.redirectTo(this).index();
	}

	@Get("/logout")
	@Public
	public void logout() {
		if (this.userSession.isLogged()) {
			this.userSession.logout();
		}

		this.result.redirectTo(HomeController.class).index();
	}
}
