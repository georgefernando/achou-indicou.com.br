package br.com.achouindicou.controller;

import java.util.List;

import javax.inject.Inject;

import br.com.achouindicou.bll.IProfileBLL;
import br.com.achouindicou.dto.ProfileDTO;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;

@Controller
public class ProfileController {

	@Inject
	private Result result;

	@Inject
	private IProfileBLL profileBLL;

	@Inject
	protected ProfileController(IProfileBLL profileBLL, Result result) {
		this.profileBLL = profileBLL;
		this.result = result;
	}

	protected ProfileController() {
		this(null, null);
	}

	@Get("/admin/profile")
	public List<ProfileDTO> index() {
		result.include("pageTitle", " - Profile(s)");
		result.include("activeProfile", "class=\"active\"");

		return this.profileBLL.get();
	}
	
	@Get("/admin/profile/add")
	public void form() {
		result.include("pageTitle", " - Adicionar Profile");
		result.include("formPageTitle", "Adicionar Profile");
		result.include("activeProfile", "class=\"active\"");
	}

	@Get("/admin/profile/edit/{id}")
	public ProfileDTO form(int id) {
		result.include("pageTitle", " - Editar Profile");
		result.include("activeProfile", "class=\"active\"");

		ProfileDTO profile = this.profileBLL.get(id);

		result.include("formPageTitle", "Editar Profile - " + profile.getName());

		return profile;
	}

	@Post("admin/profile/save")
	public void save(ProfileDTO profileDTO) {
		this.profileBLL.save(profileDTO);

		result.redirectTo(this).index();
	}
}
