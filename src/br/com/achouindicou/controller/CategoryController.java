package br.com.achouindicou.controller;

import java.util.List;

import javax.inject.Inject;

import br.com.achouindicou.bll.ICategoryBLL;
import br.com.achouindicou.dto.CategoryDTO;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;

@Controller
public class CategoryController {
	@Inject
	private Result result;

	
	@Inject
	private ICategoryBLL categoryBLL; 

	@Inject
	protected CategoryController(ICategoryBLL categoryBLL, Result result) {
		this.categoryBLL = categoryBLL;
		this.result = result;
	}
	
	protected CategoryController() {
		this(null, null);
	}

	@Get("/admin/category")
	public List<CategoryDTO> index() {
		result.include("pageTitle", " - Categoria(s)");
		result.include("activeCategory", "class=\"active\"");

		return this.categoryBLL.get();
	}

	@Get("/admin/category/add")
	public void form() {
		result.include("pageTitle", " - Adicionar Categoria");
		result.include("formPageTitle", "Adicionar Categoria");
		result.include("activeCategory", "class=\"active\"");
	}

	@Get("/admin/category/edit/{id}")
	public CategoryDTO form(int id) {
		result.include("pageTitle", " - Editar Categoria");
		result.include("activeCategory", "class=\"active\"");

		CategoryDTO category = this.categoryBLL.get(id);

		result.include("formPageTitle", "Editar Categoria - " + category.getName());

		return category;
	}

	@Post("admin/category/save")
	public void save(CategoryDTO categoryDTO) {
		this.categoryBLL.save(categoryDTO);

		result.redirectTo(this).index();
	}
}
