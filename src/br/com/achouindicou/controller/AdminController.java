package br.com.achouindicou.controller;

import javax.inject.Inject;

import br.com.achouindicou.service.UserSession;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Result;

@Controller
public class AdminController {

	@Inject
	private Result result;

	private UserSession UserSession;

	@Inject
	public AdminController(UserSession UserSession, Result result) {
		this.result = result;
		this.UserSession = UserSession;
	}

	AdminController() {
		this(null, null);
	}

	@Get("/admin")
	public void index() {
		this.result.include("userName", this.UserSession.getUser().getName());
	}
}
