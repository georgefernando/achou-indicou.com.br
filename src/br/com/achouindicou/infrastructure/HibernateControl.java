package br.com.achouindicou.infrastructure;

import javax.enterprise.context.RequestScoped;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import br.com.achouindicou.interfaces.IDBSession;

@RequestScoped
public class HibernateControl implements IDBSession{
    private final SessionFactory factory;

    public HibernateControl() {
        this.factory = new Configuration().configure().buildSessionFactory();
    }

    public Session getSession() {
        return factory.openSession();
    }
}
