package br.com.achouindicou.service;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;

import br.com.achouindicou.dto.UserDTO;

@SessionScoped
public class UserSession implements Serializable {
	private UserDTO userSession;

	public void logon(UserDTO user) {
		this.userSession = user;
	}

	public boolean isLogged() {
		return this.userSession != null;
	}

	public UserDTO getUser() {
		return this.userSession;
	}

	public void logout() {
		this.userSession = null;
	}
}
