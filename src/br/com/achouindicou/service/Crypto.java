package br.com.achouindicou.service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Crypto {
	public static String convertToHashMD5(String password) {
		try {
			MessageDigest md;

			md = MessageDigest.getInstance("MD5");

			BigInteger hash = new BigInteger(1, md.digest(password.getBytes()));

			return String.format("%32x", hash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return password;
	}
}
