package br.com.achouindicou.repository;

import java.util.List;

import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.achouindicou.interfaces.IDBSession;
import br.com.achouindicou.interfaces.IUserRepository;
import br.com.achouindicou.model.User;

public class UserRepository implements IUserRepository {
	@Inject
	private IDBSession dbSession;

	@Inject
	public UserRepository(IDBSession dbSession) {
		this.dbSession = dbSession;
	}

	UserRepository() {
		this(null);
	}

	@Override
	public List<User> get() {
		List<User> users = this.dbSession.getSession().createQuery("select u from User u", User.class).getResultList();

		return users;
	}

	@Override
	public User get(long id) {
		User user = this.dbSession.getSession().createQuery("select u from User u where u.id = :id", User.class)
				.setParameter("id", id).getSingleResult();

		return user;
	}

	@Override
	public User get(String text) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(User entity) {
		try {
			Session localSession = this.dbSession.getSession();

			Transaction tx = localSession.beginTransaction();

			if (entity.getId() == 0) {
				localSession.save(entity);
			} else {
				localSession.merge(entity);
			}

			localSession.flush();
			tx.commit();
		} catch (Exception e) {
		}
	}

	@Override
	public void delete(User entity) {
		// TODO Auto-generated method stub
	}

	@Override
	public User logon(String email, String password) {
		try {
			User user = this.dbSession.getSession()
					.createQuery("select u from User u where u.email = :email AND u.password = :password", User.class)
					.setParameter("email", email).setParameter("password", password).getSingleResult();

			return user;
		} catch (Exception e) {

		}

		return null;
	}

	@Override
	public List<User> getByProfiles(long id) {
		List<User> users = this.dbSession.getSession()
				.createQuery("select u from User u where u.profile.id = :id", User.class).setParameter("id", id)
				.getResultList();

		return users;
	}

	@Override
	public List<User> getByText(String searchText) {
		List<User> users = this.dbSession.getSession()
				.createQuery(
						"select u from User u where u.profile.id= 2 and u.active = 1 and ((u.name like :text) or (u.description like :text) or (u.address like :text) or (u.email like :text) or (u.category.name like :text))",
						User.class)
				.setParameter("text", "%" + searchText + "%").getResultList();

		return users;
	}

}
