package br.com.achouindicou.repository;

import java.util.List;

import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.achouindicou.interfaces.IDBSession;
import br.com.achouindicou.interfaces.ICommentRepository;
import br.com.achouindicou.model.Comment;

public class CommentRepository implements ICommentRepository {
	@Inject
	private IDBSession dbSession;

	@Inject
	public CommentRepository(IDBSession dbSession) {
		this.dbSession = dbSession;
	}

	CommentRepository() {
		this(null);
	}

	@Override
	public List<Comment> get() {
		List<Comment> Comments = this.dbSession.getSession().createQuery("select c from Comment c", Comment.class)
				.getResultList();

		return Comments;
	}

	@Override
	public Comment get(long id) {
		Comment category = this.dbSession.getSession()
				.createQuery("select c from Comment c where c.id = :id", Comment.class).setParameter("id", id)
				.getSingleResult();

		return category;
	}

	@Override
	public Comment get(String text) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Comment entity) {
		try {
			Session localSession = this.dbSession.getSession();

			Transaction tx = localSession.beginTransaction();

			if (entity.getId() == 0) {
				localSession.save(entity);
			} else {
				localSession.merge(entity);
			}

			localSession.flush();
			tx.commit();
		} catch (Exception e) {
		}
	}

	@Override
	public void delete(Comment entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Comment> getCommentsByUser(long id) {
		List<Comment> Comments = this.dbSession.getSession()
				.createQuery("select c from Comment c where c.user.id = :id", Comment.class).setParameter("id", id)
				.getResultList();

		return Comments;
	}
}
