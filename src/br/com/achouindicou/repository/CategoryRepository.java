package br.com.achouindicou.repository;

import java.util.List;

import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.achouindicou.interfaces.ICategoryRepository;
import br.com.achouindicou.interfaces.IDBSession;
import br.com.achouindicou.model.Category;

public class CategoryRepository implements ICategoryRepository {
	@Inject
	private IDBSession dbSession;

	@Inject
	public CategoryRepository(IDBSession dbSession) {
		this.dbSession = dbSession;
	}

	CategoryRepository() {
		this(null);
	}

	@Override
	public List<Category> get() {
		List<Category> categories = this.dbSession.getSession()
				.createQuery("select cat from Category cat", Category.class).getResultList();

		return categories;
	}

	@Override
	public Category get(long id) {
		Category category = this.dbSession.getSession()
				.createQuery("select cat from Category cat where cat.id = :id", Category.class).setParameter("id", id)
				.getSingleResult();

		return category;
	}

	@Override
	public Category get(String text) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Category entity) {
		try {
			Session localSession = this.dbSession.getSession();
			Transaction tx = localSession.beginTransaction();

			if (entity.getId() == 0) {
				localSession.save(entity);
			} else {
				localSession.merge(entity);
			}

			localSession.flush();
			tx.commit();
		} catch (Exception e) {
		}
	}

	@Override
	public void delete(Category entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Category> getActives() {
		List<Category> categories = this.dbSession.getSession()
				.createQuery("select cat from Category cat where cat.active=1", Category.class).getResultList();

		return categories;
	}

}
