package br.com.achouindicou.repository;

import java.util.List;

import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.achouindicou.interfaces.IDBSession;
import br.com.achouindicou.interfaces.IProfileRepository;
import br.com.achouindicou.model.Profile;

public class ProfileRepository implements IProfileRepository {
	@Inject
	private IDBSession dbSession;

	@Inject
	public ProfileRepository(IDBSession dbSession) {
		this.dbSession = dbSession;
	}

	ProfileRepository() {
		this(null);
	}

	@Override
	public List<Profile> get() {
		List<Profile> profiles = this.dbSession.getSession().createQuery("select p from Profile p", Profile.class)
				.getResultList();

		return profiles;
	}

	@Override
	public Profile get(long id) {
		Profile category = this.dbSession.getSession()
				.createQuery("select p from Profile p where p.id = :id", Profile.class).setParameter("id", id)
				.getSingleResult();

		return category;
	}

	@Override
	public Profile get(String text) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Profile entity) {
		try {
			Session localSession = this.dbSession.getSession();

			Transaction tx = localSession.beginTransaction();

			if (entity.getId() == 0) {
				localSession.save(entity);
			} else {
				localSession.merge(entity);
			}

			localSession.flush();
			tx.commit();
		} catch (Exception e) {
		}
	}

	@Override
	public void delete(Profile entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Profile> getActives() {
		List<Profile> profiles = this.dbSession.getSession()
				.createQuery("select p from Profile p where p.active=1", Profile.class).getResultList();

		return profiles;
	}

}
