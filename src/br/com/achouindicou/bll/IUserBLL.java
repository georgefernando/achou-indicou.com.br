package br.com.achouindicou.bll;

import java.util.List;

import br.com.achouindicou.dto.UserDTO;

public interface IUserBLL extends IBaseBLL<UserDTO> {
	List<UserDTO> getActives();

	UserDTO logon(String email, String password);

	List<UserDTO> getByProfiles(long id);
	
	List<UserDTO> getByText(String searchText);
}
