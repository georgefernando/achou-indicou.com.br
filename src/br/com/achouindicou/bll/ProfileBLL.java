package br.com.achouindicou.bll;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import br.com.achouindicou.dto.ProfileDTO;
import br.com.achouindicou.interfaces.IProfileRepository;
import br.com.achouindicou.model.Profile;

public class ProfileBLL implements IProfileBLL {
	@Inject
	IProfileRepository profileRepository;

	@Inject
	public ProfileBLL(IProfileRepository profileRepository) {
		this.profileRepository = profileRepository;
	}

	ProfileBLL() {
		this(null);
	}

	@Override
	public List<ProfileDTO> get() {
		List<Profile> profiles = this.profileRepository.get();

		List<ProfileDTO> profilesDTO = new ArrayList<>();

		java.lang.reflect.Type targetListType = new TypeToken<List<ProfileDTO>>() {
		}.getType();

		ModelMapper mapper = new ModelMapper();

		profilesDTO = mapper.map(profiles, targetListType);

		return profilesDTO;
	}

	@Override
	public ProfileDTO get(long id) {
		Profile category = this.profileRepository.get(id);

		ModelMapper mapper = new ModelMapper();

		ProfileDTO profileDTO = mapper.map(category, ProfileDTO.class);

		return profileDTO;
	}

	@Override
	public ProfileDTO get(String text) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(ProfileDTO entity) {
		ModelMapper mapper = new ModelMapper();

		Profile profle = mapper.map(entity, Profile.class);
		
		this.profileRepository.save(profle);

	}

	@Override
	public void delete(ProfileDTO entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<ProfileDTO> getActives() {
		List<Profile> profiles = this.profileRepository.getActives();

		List<ProfileDTO> profilesDTO = new ArrayList<>();

		java.lang.reflect.Type targetListType = new TypeToken<List<ProfileDTO>>() {
		}.getType();

		ModelMapper mapper = new ModelMapper();

		profilesDTO = mapper.map(profiles, targetListType);

		return profilesDTO;
	}

}
