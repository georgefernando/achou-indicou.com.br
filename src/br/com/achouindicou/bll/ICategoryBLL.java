package br.com.achouindicou.bll;

import java.util.List;

import br.com.achouindicou.dto.CategoryDTO;

public interface ICategoryBLL extends IBaseBLL<CategoryDTO> {
	List<CategoryDTO> getActives();
}
