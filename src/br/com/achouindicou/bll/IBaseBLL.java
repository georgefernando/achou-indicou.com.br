package br.com.achouindicou.bll;

import java.util.List;

public interface IBaseBLL<TEntity> {
	List<TEntity> get();

	TEntity get(long id);

	TEntity get(String text);

	void save(TEntity entity);

	void delete(TEntity entity);
}
