package br.com.achouindicou.bll;

import java.util.List;

import br.com.achouindicou.dto.CommentDTO;

public interface ICommentBLL extends IBaseBLL<CommentDTO> {
	List<CommentDTO> getCommentsByUser(long id);
}
