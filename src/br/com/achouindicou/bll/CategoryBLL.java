package br.com.achouindicou.bll;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import br.com.achouindicou.dto.CategoryDTO;
import br.com.achouindicou.interfaces.ICategoryRepository;
import br.com.achouindicou.model.Category;;

public class CategoryBLL implements ICategoryBLL {
	@Inject
	private ICategoryRepository categoryRep;

	@Inject
	public CategoryBLL(ICategoryRepository categoryRepository) {
		this.categoryRep = categoryRepository;
	}

	CategoryBLL() {
		this(null);
	}

	@Override
	public List<CategoryDTO> get() {
		List<Category> categories = this.categoryRep.get();

		List<CategoryDTO> categoriesDTO = new ArrayList<>();

		java.lang.reflect.Type targetListType = new TypeToken<List<CategoryDTO>>() {
		}.getType();

		ModelMapper mapper = new ModelMapper();

		categoriesDTO = mapper.map(categories, targetListType);

		return categoriesDTO;
	}

	@Override
	public CategoryDTO get(long id) {
		Category category = this.categoryRep.get(id);

		ModelMapper mapper = new ModelMapper();

		CategoryDTO categoryDTO = mapper.map(category, CategoryDTO.class);

		return categoryDTO;
	}

	@Override
	public CategoryDTO get(String text) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(CategoryDTO categoryDTO) {
		ModelMapper mapper = new ModelMapper();

		Category category = mapper.map(categoryDTO, Category.class);
		
		this.categoryRep.save(category);
	}

	@Override
	public void delete(CategoryDTO entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<CategoryDTO> getActives() {
		List<Category> categories = this.categoryRep.getActives();

		List<CategoryDTO> categoriesDTO = new ArrayList<>();

		java.lang.reflect.Type targetListType = new TypeToken<List<CategoryDTO>>() {
		}.getType();

		ModelMapper mapper = new ModelMapper();

		categoriesDTO = mapper.map(categories, targetListType);

		return categoriesDTO;
	}

}
