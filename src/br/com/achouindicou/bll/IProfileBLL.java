package br.com.achouindicou.bll;

import java.util.List;

import br.com.achouindicou.dto.ProfileDTO;

public interface IProfileBLL extends IBaseBLL<ProfileDTO> {
	List<ProfileDTO> getActives();
}
