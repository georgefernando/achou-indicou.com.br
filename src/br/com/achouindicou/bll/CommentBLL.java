package br.com.achouindicou.bll;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import br.com.achouindicou.dto.CommentDTO;
import br.com.achouindicou.interfaces.ICommentRepository;
import br.com.achouindicou.model.Comment;

public class CommentBLL implements ICommentBLL {
	@Inject
	ICommentRepository CommentRepository;

	@Inject
	public CommentBLL(ICommentRepository CommentRepository) {
		this.CommentRepository = CommentRepository;
	}

	CommentBLL() {
		this(null);
	}

	@Override
	public List<CommentDTO> get() {
		List<Comment> Comments = this.CommentRepository.get();

		List<CommentDTO> CommentsDTO = new ArrayList<>();

		java.lang.reflect.Type targetListType = new TypeToken<List<CommentDTO>>() {
		}.getType();

		ModelMapper mapper = new ModelMapper();

		CommentsDTO = mapper.map(Comments, targetListType);

		return CommentsDTO;
	}

	@Override
	public CommentDTO get(long id) {
		Comment comment = this.CommentRepository.get(id);

		ModelMapper mapper = new ModelMapper();

		CommentDTO CommentDTO = mapper.map(comment, CommentDTO.class);

		return CommentDTO;
	}

	@Override
	public CommentDTO get(String text) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(CommentDTO entity) {
		ModelMapper mapper = new ModelMapper();

		Comment comment = mapper.map(entity, Comment.class);

		this.CommentRepository.save(comment);

	}

	@Override
	public void delete(CommentDTO entity) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<CommentDTO> getCommentsByUser(long id) {
		List<Comment> Comments = this.CommentRepository.getCommentsByUser(id);

		List<CommentDTO> CommentsDTO = new ArrayList<>();

		java.lang.reflect.Type targetListType = new TypeToken<List<CommentDTO>>() {
		}.getType();

		ModelMapper mapper = new ModelMapper();

		CommentsDTO = mapper.map(Comments, targetListType);

		return CommentsDTO;
	}
}
