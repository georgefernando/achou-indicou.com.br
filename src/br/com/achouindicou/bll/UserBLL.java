package br.com.achouindicou.bll;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import br.com.achouindicou.dto.UserDTO;
import br.com.achouindicou.interfaces.ICommentRepository;
import br.com.achouindicou.interfaces.IUserRepository;
import br.com.achouindicou.model.Comment;
import br.com.achouindicou.model.User;
import br.com.achouindicou.service.Crypto;

public class UserBLL implements IUserBLL {
	@Inject
	private IUserRepository userRep;

	@Inject
	private ICommentRepository commentRep;

	@Inject
	public UserBLL(IUserRepository userRepository, ICommentRepository commentRep) {
		this.userRep = userRepository;
		this.commentRep = commentRep;
	}

	UserBLL() {
		this(null, null);
	}

	@Override
	public List<UserDTO> get() {
		List<User> users = this.userRep.get();

		List<UserDTO> usersDTO = new ArrayList<>();

		java.lang.reflect.Type targetListType = new TypeToken<List<UserDTO>>() {
		}.getType();

		ModelMapper mapper = new ModelMapper();

		usersDTO = mapper.map(users, targetListType);

		return usersDTO;
	}

	@Override
	public UserDTO get(long id) {
		User user = this.userRep.get(id);

		ModelMapper mapper = new ModelMapper();

		UserDTO userDTO = mapper.map(user, UserDTO.class);

		List<Comment> comments = this.commentRep.getCommentsByUser(id);

		if (comments != null && !comments.isEmpty()) {
			double good = 0, bad = 0, neutral = 0, total = comments.size();

			for (Comment c : comments) {
				switch (c.getClassification()) {
				case "G":
					good++;
					break;
				case "B":
					bad++;
					break;

				case "N":
					neutral++;
					break;
				}
			}

			if (good > 0) {
				userDTO.setGod((good / total) * 100);
			}

			if (bad > 0) {
				userDTO.setBad((bad / total) * 100);
			}

			if (neutral > 0) {
				userDTO.setNeutral((neutral / total) * 100);
			}
		}

		return userDTO;
	}

	@Override
	public UserDTO get(String text) {
		return null;
	}

	@Override
	public void save(UserDTO userDTO) {
		ModelMapper mapper = new ModelMapper();

		User user = mapper.map(userDTO, User.class);

		user.setPassword(Crypto.convertToHashMD5(user.getPassword()));

		this.userRep.save(user);
	}

	@Override
	public void delete(UserDTO entity) {
	}

	@Override
	public List<UserDTO> getActives() {
		return null;
	}

	@Override
	public UserDTO logon(String email, String password) {
		User user = this.userRep.logon(email, Crypto.convertToHashMD5(password));

		UserDTO userDTO = null;

		if (user != null) {
			ModelMapper mapper = new ModelMapper();

			userDTO = mapper.map(user, UserDTO.class);
		}

		return userDTO;
	}

	@Override
	public List<UserDTO> getByProfiles(long id) {
		List<User> users = this.userRep.getByProfiles(id);

		List<UserDTO> usersDTO = new ArrayList<>();

		java.lang.reflect.Type targetListType = new TypeToken<List<UserDTO>>() {
		}.getType();

		ModelMapper mapper = new ModelMapper();

		usersDTO = mapper.map(users, targetListType);

		for (Iterator<UserDTO> item = usersDTO.listIterator(); item.hasNext();) {
			if (!item.next().isActive()) {
				item.remove();
			}
		}

		return usersDTO;
	}

	@Override
	public List<UserDTO> getByText(String searchText) {
		List<User> users = this.userRep.getByText(searchText);

		java.lang.reflect.Type targetListType = new TypeToken<List<UserDTO>>() {
		}.getType();

		ModelMapper mapper = new ModelMapper();

		List<UserDTO> usersDTO = new ArrayList<>();

		usersDTO = mapper.map(users, targetListType);

		return usersDTO;
	}

}
