package br.com.achouindicou.interfaces;

import java.util.List;

import br.com.achouindicou.model.Comment;

public interface ICommentRepository extends IBaseRepository<Comment> {
	List<Comment> getCommentsByUser(long id);
}
