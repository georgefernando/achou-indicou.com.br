package br.com.achouindicou.interfaces;

import java.util.List;

public interface IBaseRepository<TEntity> {
	List<TEntity> get();

	TEntity get(long id);

	TEntity get(String text);

	void save(TEntity entity);

	void delete(TEntity entity);
}
