package br.com.achouindicou.interfaces;

import java.util.List;

import br.com.achouindicou.model.User;

public interface IUserRepository extends IBaseRepository<User> {
	User logon(String email, String password);

	List<User> getByProfiles(long id);

	List<User> getByText(String searchText);
}
