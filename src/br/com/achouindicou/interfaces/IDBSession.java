package br.com.achouindicou.interfaces;

import org.hibernate.Session;

public interface IDBSession {
	Session getSession();
}
