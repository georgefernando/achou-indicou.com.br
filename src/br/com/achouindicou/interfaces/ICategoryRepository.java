package br.com.achouindicou.interfaces;

import java.util.List;

import br.com.achouindicou.model.Category;

public interface ICategoryRepository extends IBaseRepository<Category> {
	List<Category> getActives();
}
