package br.com.achouindicou.interfaces;

import java.util.List;

import br.com.achouindicou.model.Profile;

public interface IProfileRepository extends IBaseRepository<Profile> {
	List<Profile> getActives();
}
