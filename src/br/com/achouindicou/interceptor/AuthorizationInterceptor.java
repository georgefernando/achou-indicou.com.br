package br.com.achouindicou.interceptor;

import javax.inject.Inject;

import br.com.achouindicou.controller.LoginController;
import br.com.achouindicou.service.UserSession;
import br.com.caelum.vraptor.Accepts;
import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.controller.ControllerMethod;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;
import br.com.caelum.vraptor.view.Results;

@Intercepts(after = AuthenticationInterceptor.class)
public class AuthorizationInterceptor {
	private UserSession userSession;
	private Result result;

	AuthorizationInterceptor() {

	}

	@Inject
	public AuthorizationInterceptor(UserSession userSession, Result result) {
		this.result = result;
		this.userSession = userSession;
	}

	@Accepts
	public boolean accepts(ControllerMethod method) {
		return !method.containsAnnotation(Public.class);
	}

	@AroundCall
	public void authorizes(SimpleInterceptorStack stack, ControllerMethod method) {
		if (this.userSession.isLogged()) {
			if (canAaccess(method)) {
				stack.next();
			} else {
				this.result.use(Results.http()).sendError(401, "Acessão não autorizado.");
			}
		} else {
			this.result.redirectTo(LoginController.class).index();
		}
	}

	private boolean canAaccess(ControllerMethod method) {
		return this.userSession.getUser().getProfile().getName().toLowerCase().equals("administrador");
	}
}
