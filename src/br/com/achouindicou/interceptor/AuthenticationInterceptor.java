package br.com.achouindicou.interceptor;

import javax.inject.Inject;

import br.com.achouindicou.controller.LoginController;
import br.com.achouindicou.service.UserSession;
import br.com.caelum.vraptor.Accepts;
import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.controller.ControllerMethod;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;

@Intercepts
public class AuthenticationInterceptor {
	private UserSession userSession;
	private Result result;

	AuthenticationInterceptor() {
		this(null, null);
	}

	@Inject
	public AuthenticationInterceptor(UserSession userSession, Result result) {
		this.userSession = userSession;
		this.result = result;
	}

	@AroundCall
	public void authenticates(SimpleInterceptorStack stack) {
		if (this.userSession.isLogged()) {
			stack.next();
		} else {
			this.result.redirectTo(LoginController.class).index();
		}
	}

	@Accepts
	public boolean accepts(ControllerMethod method) {
		return !method.containsAnnotation(Public.class);
	}
}
