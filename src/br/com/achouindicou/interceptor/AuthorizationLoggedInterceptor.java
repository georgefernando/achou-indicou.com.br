package br.com.achouindicou.interceptor;

import javax.inject.Inject;

import br.com.achouindicou.controller.LoginController;
import br.com.achouindicou.service.UserSession;
import br.com.caelum.vraptor.Accepts;
import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.controller.ControllerMethod;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;

@Intercepts(after = AuthenticationInterceptor.class)
public class AuthorizationLoggedInterceptor {
	private UserSession userSession;
	private Result result;

	AuthorizationLoggedInterceptor() {

	}

	@Inject
	public AuthorizationLoggedInterceptor(UserSession userSession, Result result) {
		this.result = result;
		this.userSession = userSession;
	}

	@Accepts
	public boolean accepts(ControllerMethod method) {
		return method.containsAnnotation(Logged.class);
	}

	@AroundCall
	public void authorizes(SimpleInterceptorStack stack, ControllerMethod method) {
		if (this.userSession.isLogged()) {
			stack.next();
		} else {
			this.result.redirectTo(LoginController.class).index();
		}
	}
}
